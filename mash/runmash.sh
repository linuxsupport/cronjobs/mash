#!/bin/bash

CONFIG='/local/config.json'
OUTPUT='/alloc/output.json'

KOJI_GPG_KEYS="2F4F9F6E 246F2A64"
MASH_VIEWURL="http://linuxsoft.cern.ch/internal/repos"
TMP_NAME="$(mktemp -u 'XXXXXXXX')"
MASH_DIR="/staging/mash.${TMP_NAME}"
TEMP_DIR="/staging/tmp.${TMP_NAME}"
CACHE_DIR="/staging/cache"
MASH_BUILDS="/staging/buildlists"
REPO_DIR="/repo"
RSYNC_DEST="${REPO_DIR}/internal/repos"
RSYNC_PUB="${REPO_DIR}/repos"
COMPS_URL="https://gitlab.cern.ch/linuxsupport/comps.xml/raw/master"

log () {
  mapfile IN
  cat << EOF | tr -d '\n'; echo
{ "koji_tag": "${TAG}",
  ${IN[@]}
}
EOF
}

error () {
  mapfile IN
  cat << EOF | log
    "message_type": "error",
    ${IN[@]}
EOF
}

finish () {
  echo "Deleting ${MASH_DIR} and ${TEMP_DIR}"
  rm -rf ${MASH_DIR} ${TEMP_DIR}
  rm -f ${LOCKFILE}
  cat << EOF | log
  "message_type": "lock",
  "message": "Lock released"
EOF
}

if [ ! -f $CONFIG ]; then
  RET=-1
  cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "no config file found"
EOF
  exit $RET
fi

# Read the job parameters
TAG="`jq -r '.TAG // ""' $CONFIG`"
ARCHES="`jq -r '.ARCHES // "x86_64"' $CONFIG`"
FORCE="`jq -r '.FORCE // ""' $CONFIG`"

# Replace i686 arch with i386
ARCHES="${ARCHES/i686/i386}"

conf=$(cat $CONFIG)
cat << EOF | log
  "message_type": "start",
  "message": "Running Mash for ${TAG}: ${ARCHES}",
  "config": "${conf}"
EOF

# See if this tag matches one of the regexp from the list of excluded tags
echo "${TAG}" | grep -q -f /root/exclude.conf && EXCLUDE=0 || EXCLUDE=1
if [[ $EXCLUDE -eq 0 ]]; then
  cat << EOF | log
  "message_type": "result",
  "message": "Nothing to do, excluded tag",
  "exit_code": 0
EOF
  exit 0;
fi

# See if this tag matches one of the regexp from the list of public tags
echo "${TAG}" | grep -q -f /root/public.conf && PUBLIC=1 || PUBLIC=0

# *-testing tags should only build latest packages
echo "${TAG}" | grep -q -e '-testing' && ONLY_LATEST="True" || ONLY_LATEST="False"

# See if this tag matches one of the regexp from the list of multilib tags
echo "${TAG}" | grep -q -f /root/multilib.conf && MULTILIB=1 || MULTILIB=0

SHORTTAG=`echo "${TAG}" | cut -d'-' -f1`
BUILDHOST=`awk -F "=" '/server/ {print $2}' /etc/koji.conf | tr -d ' '`
REPODIR=`awk -F "=" '/topurl/ {print $2}' /etc/koji.conf | tr -d ' '`

cat << EOF > /etc/mash/${TAG}.mash
[${TAG}]
buildhost = ${BUILDHOST}
repodir = ${REPODIR}
rpm_path = ${MASH_DIR}/${TAG}/%(arch)s/os/Packages
repodata_path = ${MASH_DIR}/${TAG}/%(arch)s/os/
cachedir = ${CACHE_DIR}
workdir = ${TEMP_DIR}
outputdir = ${MASH_DIR}
source_path = source/SRPMS
debuginfo = True
multilib = ${MULTILIB}
multilib_method = devel
tag = ${TAG}
inherit = False
keys = ${KOJI_GPG_KEYS}
strict_keys = False
use_repoview = False
repoviewurl = ${MASH_VIEWURL}/${TAG}/%(arch)s/os/
repoviewtitle = "${TAG^^}"
arches = ${ARCHES}
delta = False
latest = ${ONLY_LATEST}
EOF


# nss fix for el6/el7
export NSS_STRICT_NOFORK=DISABLED

# Make destination directory if it doesn't exist
[[ ! -d "${RSYNC_DEST}/${TAG}" ]] && mkdir -p ${RSYNC_DEST}/${TAG}


# Locking mechanism:
# We don't want mash to run concurrently, so each tag (except db*) gets a lockfile. Mash doesn't run unless
# it can aquire the lock. However, it doesn't make sense to have multiple tasks waiting for the same tag, as
# the next one to run will handle all the updates anyway.
#
# This is why we also create a WAITFILE just before aquiring the lock, and delete it just afterwards. If the
# WAITFILE already exists (and it's not stale), it means there's already a mash job waiting for that particular task,
# and therefore this task doesn't need to wait as well.

# DB tags are huge, so don't try to do more than one at a time
echo "${TAG}" | grep -q -e '^db[0-9]-' && LOCKFILE="${RSYNC_DEST}/db-mash.lock" || LOCKFILE="${RSYNC_DEST}/${TAG}/mash.lock"
WAITFILE="${RSYNC_DEST}/${TAG}/mash.wait"

if [ -f "${WAITFILE}" ]; then
  MODTIME=`stat -c "%Y" "${WAITFILE}"`
  NOW=`date +%s`
  AGE=$(($NOW - $MODTIME))
  if [[ $AGE -lt 21600 ]]; then
    cat << EOF | log
    "message_type": "lock",
    "message": "Somebody already waiting, we're not needed"
EOF
    exit 0;
  fi
fi
touch "${WAITFILE}"

# Make sure we always release the lock when the script exits
trap finish EXIT

# Grab a lock. Lock is stale if it's more than 6h old
cat << EOF | log
  "message_type": "lock",
  "message": "Grabbing lock"
EOF
lockfile -l 21600 ${LOCKFILE}
rm -f ${WAITFILE}
cat << EOF | log
  "message_type": "lock",
  "message": "Lock acquired"
EOF

# Record start time
start=`date +%s`
RUN_MASH=0

# Get list of tagged builds
/usr/bin/koji list-tagged ${TAG} | tail -n +3 | sort > /tmp/${TAG}.buildlist

# Figure out if something has changed or not
if [ ! -f ${MASH_BUILDS}/${TAG}.buildlist ]; then
  RUN_MASH=1
else
  NEWBUILDS=`/usr/bin/diff --changed-group-format='%>' --unchanged-group-format='' ${MASH_BUILDS}/${TAG}.buildlist /tmp/${TAG}.buildlist`
  OLDBUILDS=`/usr/bin/diff --changed-group-format='%<' --unchanged-group-format='' ${MASH_BUILDS}/${TAG}.buildlist /tmp/${TAG}.buildlist`

  if [[ "$NEWBUILDS" != "" || "$OLDBUILDS" != "" || "$FORCE" == "true" ]]; then
    RUN_MASH=1
  fi
fi

# Try to download comps.xml file, fail on 404
curl -fs "${COMPS_URL}/${SHORTTAG}.xml" -o comps.xml
if [[ $? -eq 0 ]]; then
  COMPS="-f /root/comps.xml"
  OLDCOMPS=`md5sum ${RSYNC_DEST}/${TAG}/${ARCHES}/os/repodata/*comps.xml | awk '{print $1}'`
  NEWCOMPS=`md5sum /root/comps.xml | awk '{print $1}'`
  if [[ "$NEWCOMPS" != "$OLDCOMPS" ]]; then
    RUN_MASH=1
  fi
fi

# Don't do anything if there are no changes
if [[ $RUN_MASH -eq 0 ]]; then
  cat << EOF | log
  "message_type": "result",
  "message": "Nothing to do",
  "exit_code": 0
EOF
  exit 0;
fi

# Run mash
/usr/bin/mash \
  -c /etc/mash/mash.conf \
  ${COMPS} ${TAG}
RET=$?

if [[ $RET -ne 0 ]]; then
  cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "mash failed"
EOF
  exit $RET
fi


if [[ $PUBLIC -eq 1 ]]; then
  echo "Require all granted" > ${MASH_DIR}/${TAG}/.htaccess
fi



# Run rsync, first the RPMs, then the repodata and then delete
/usr/bin/rsync \
  -vrlptD \
  --exclude 'repodata' \
  ${MASH_DIR}/${TAG} \
  ${RSYNC_DEST} \
&& \
/usr/bin/rsync \
  -vrlptD \
  --exclude '.last_run_time' \
  --exclude 'mash.lock' \
  --delete-after \
  ${MASH_DIR}/${TAG} \
  ${RSYNC_DEST}
RET=$?

if [[ $RET -ne 0 ]]; then
  cat << EOF | error | tee $OUTPUT
    "exit_code": $RET,
    "error": "rsync failed"
EOF
  exit $RET
fi


# Create link from the public part, if needed
if [[ $PUBLIC -eq 1 ]]; then
  [[ ! -d "${RSYNC_PUB}" ]] && mkdir -p ${RSYNC_PUB}
  ln -sfr ${RSYNC_DEST}/${TAG} ${RSYNC_PUB}
else
  [ -h ${RSYNC_PUB}/${TAG} ] && rm ${RSYNC_PUB}/${TAG}
fi

# Save the new buildlist
mv /tmp/${TAG}.buildlist ${MASH_BUILDS}/${TAG}.buildlist

# Set the last run time
date --iso-8601=seconds > ${RSYNC_DEST}/${TAG}/.last_run_time

end=`date +%s`
runtime=$((end-start))


while read -r line; do
  build=`echo $line | /usr/bin/awk '{print $1}'`
  author=`echo $line | /usr/bin/awk '{print $3}'`
  [[ "$build" == "" ]] && break
  cat << EOF | log
    "message_type": "tagged",
    "build": "${build}",
    "author": "${author}"
EOF
done <<< "${NEWBUILDS}"

while read -r line; do
  build=`echo $line | /usr/bin/awk '{print $1}'`
  [[ "$build" == "" ]] && break
  cat << EOF | log
    "message_type": "untagged",
    "build": "${build}"
EOF
done <<< "${OLDBUILDS}"

cat << EOF | log
  "message_type": "result",
  "runtime_secs": ${runtime},
  "exit_code": ${RET}
EOF
