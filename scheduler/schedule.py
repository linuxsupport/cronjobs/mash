#!/usr/bin/python

import nomad
from nomad.api import exceptions
import yaml
import json
import koji
import re
import sys
from datetime import datetime
import time
import os
import base64

CONFIG = '/root/config.yaml'
JOBNAME = os.getenv('MASH_JOB', 'mash')
STATUS_CHECK_SECS = os.getenv('STATUS_CHECKS', 5)

REGEX_TAG_QA = '-qa'
REGEX_TAG_TESTING = '-testing'
REGEX_TAG_STABLE = '-stable'


def log(**kwargs):
  print(json.dumps(kwargs))

def debug(**kwargs):
  kwargs.update({'message_type': 'debug'})
  log(**kwargs)

def error(**kwargs):
  kwargs.update({'message_type': 'error'})
  log(**kwargs)



try:
  with open('/secrets/token') as f:
    nomad_token = f.read().strip('\n')
except IOError as e:
  error(comment='Unable to read the Nomad token', exception=str(e))
  sys.exit(1)


debug(comment='Connecting to Nomad at: {}'.format(os.getenv('NOMAD_ADDR', None)))
n = nomad.Nomad(secure=True, token=nomad_token, timeout=5, verify=False)

try:
  with open(CONFIG) as f:
    config = yaml.load(f.read())
except IOError as e:
  error(comment='Unable to read configuration file {}'.format(CONFIG), exception=str(e))
  sys.exit(1)

# Don't fail even with an empty config file
if not config:
  config = {}

# Set up a koji session
koji_options = koji.read_config('koji')
koji_session = koji.ClientSession(koji_options['server'], koji.grab_session_options(koji_options))

try:
  tags = koji_session.listTags()
except Exception as e:
  error(comment='Unable to connect to Koji: {}'.format(koji_options['server']), exception=str(e))
  sys.exit(1)

try:
  if config['regex_tag_excludes']:
    # Remove excluded tags
    tags = [x for x in tags if not re.search('|'.join(config['regex_tag_excludes']), x['name'])]
except KeyError:
  pass

tags_qa      = [x['name'] for x in tags if re.search(config.get('regex_tag_qa',      REGEX_TAG_QA),      x['name'])]
tags_testing = [x['name'] for x in tags if re.search(config.get('regex_tag_testing', REGEX_TAG_TESTING), x['name'])]
tags_stable  = [x['name'] for x in tags if re.search(config.get('regex_tag_stable',  REGEX_TAG_STABLE),  x['name'])]
tags_all = tags_testing + tags_qa + tags_stable

jobs = {}
for tag in tags_all:
  list_tagged = koji_session.listTagged(tag, latest=True)

  # Figure out the arches for this tag. Look at the builds
  # to find the tag they were built under and check that
  # tags arches. We only need the first one, all the others
  # will be the same. We default to 'x86_64 aarch64'.
  arches = 'x86_64 aarch64'
  for build in list_tagged:
    if not build['task_id']:
      continue

    info = koji_session.getTaskInfo(build['task_id'], request=True)
    target = None
    request = info['request']
    if info['method'] in ('build', 'maven'):
      # request is (source-url, build-target, map-of-other-options)
      if request[1]:
        target = koji_session.getBuildTarget(request[1])
    elif info['method'] == 'winbuild':
      # request is (vm-name, source-url, build-target, map-of-other-options)
      if request[2]:
        target = koji_session.getBuildTarget(request[2])
    if not target:
      continue

    target_info = koji_session.getTag(target['build_tag_name'])
    if target_info['arches']:
      arches = target_info['arches']
      break

  payload = {
    'TAG': tag,
    'ARCHES': arches,
  }
  enc_payload = base64.b64encode(json.dumps(payload)).decode()

  try:
    job = n.job.dispatch_job(JOBNAME, meta={"PARENT_JOB": os.getenv('NOMAD_JOB_NAME', None)}, payload=enc_payload)
    log(comment='dispatched job', tag=tag, job=job, payload=payload)
    jobs[job['DispatchedJobID']] = 'created'

  except (exceptions.URLNotFoundNomadException, exceptions.BaseNomadException) as e:
    error(comment='Error dispatching job', exception=e.nomad_resp.text)
    raise e

log(comment='Waiting for jobs to finish')
results = {}
while jobs:
    for j in jobs.keys():
        state = n.job.get_job(j)
        log(comment='Job state', job=j, state=state['Status'])
        if state['Status'] == 'dead':
            del jobs[j]
        else:
            jobs[j] = state['Status']
    time.sleep(STATUS_CHECK_SECS)

log(comment='All done!')
