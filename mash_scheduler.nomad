job "${PREFIX}_mash_scheduler" {
  datacenters = ["*"]

  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  reschedule {
    attempts       = 132
    interval       = "11h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_mash_scheduler" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/mash/scheduler:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_mash"
        }
      }
      volumes = [
        "/etc/nomad/submit_token:/secrets/token"
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      MASH_JOB = "${PREFIX}_mash"
    }

    resources {
      cpu = 100 # Mhz
      memory = 128 # MB
    }
  }
}
