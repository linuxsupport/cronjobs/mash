job "${PREFIX}_mash" {
  datacenters = ["*"]

  type = "batch"

  spread {
    attribute = "${node.unique.name}"
  }

  parameterized {
    payload = "required"
    meta_required = ["PARENT_JOB"]
  }

  reschedule {
    attempts       = 132
    interval       = "11h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  priority = 60

  task "${PREFIX}_mash" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/mash/mash:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_mash"
        }
      }
      volumes = [
        "$MP_FINAL:/repo",
        "$MP_STAGING:/staging",
      ]
    }

    dispatch_payload {
      file = "config.json"
    }

    resources {
      cpu = 5000 # Mhz
      memory = 512 # MB
    }
  }
}
